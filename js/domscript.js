var list_storage = [];
var temp_hidden_elem = { 'post': [], 'shared': [], 'comment': [] };
var _interval = null;

init();

function init(){
    $(document).ready(function(){
        addListenerHomeBtn('test');
        //if('test' in list_storage && _interval == null){
        if(_interval == null){
            _interval = setInterval(function () {
                filter();
                //console.log('wew');
            }, 1000);
        }
    });

    chrome.runtime.onConnect.addListener(function(port) {
    if(port.name == "fbfilter"){
        port.onMessage.addListener(function(request) {
            switch (request.comm) {
                case "init list storage":
                    //console.log(list_storage)
                    // if(!('test' in list_storage)){
                    //     temp_hidden_elem = { 'post': [], 'shared': [], 'comment': [] };
                    // }
                    // addListenerHomeBtn('test');
                    // //if('test' in list_storage && _interval == null){
                    // if(_interval == null){
                    //     _interval = setInterval(function () {
                    //         filter();
                    //         console.log('wew');
                    //     }, 1000);
                    // }
                    var resp = { list: temp_hidden_elem, counter: getFilteredPostsTotal(temp_hidden_elem) };
                    port.postMessage(resp);
                    break;
                    
                case "filter feeds":
                    filter();
                    var resp = { counter: getFilteredPostsTotal(temp_hidden_elem) };
                    port.postMessage(resp);
                    break;

                case "undo all filters":
                    undoAllFilter('test');
                    var resp = { counter: getFilteredPostsTotal(temp_hidden_elem) };
                    port.postMessage(resp);
                    break;

                case "delete a filter":
                    deleteAFilter(request.id, request.type, 'test');
                    var resp = { counter: getFilteredPostsTotal(temp_hidden_elem) };
                    port.postMessage(resp);
                    break;

                case "list update":
                    var resp = { list: temp_hidden_elem, counter: getFilteredPostsTotal(temp_hidden_elem) };
                    port.postMessage(resp);
                    break;

                case "show filtered post":
                    showFilteredPost(request.index, true, request.type, 'test');
                    //port.postMessage({ counter: getFilteredPostsTotal(temp_hidden_elem) });
                    break;

                case "hide filtered post":
                    showFilteredPost(request.index, false, request.type, 'test');
                    //port.postMessage({ counter: getFilteredPostsTotal(temp_hidden_elem) });
                    break;
                
                case "remove filters by type":
                    removeFiltersByType(request.type, 'test');
                    var resp = { counter: getFilteredPostsTotal(temp_hidden_elem) };
                    port.postMessage(resp);
                    break;

                default:
            }
            
            
        });
    }
    
    return true;
    
    });
}


// chrome.runtime.onMessage.addListener(
//     function (request, sender, sendResponse) {
//         switch (request.comm) {
//             case "filter feeds":
//                 console.log('dom')
//                 filter();
//                 break;

//             case "undo all filters":
//                 undoAllFilter();
//                 break;

//             case "delete a filter":
//                 deleteAFilter(request.id, request.type);
//                 break;

//             case "list update":
//                 sendResponse({ list: temp_hidden_elem });
//                 break;

//             case "show filtered post":
//                 showFilteredPost(request.index, true, request.type);
//                 break;

//             case "hide filtered post":
//                 showFilteredPost(request.index, false, request.type);
//                 break;
            
//             case "remove filters by type":
//                 removeFiltersByType(request.type);
//                 break;

//             default:
//         }

//         sendResponse({ counter: temp_hidden_elem.post.length });
//     });

// $(document).ready(function () {
//     addListenerHomeBtn();
//     setInterval(function () {
//         filter();
//         //console.log('wew');
//     }, 1000);

//     //testxhr();
// });

function getFilteredPostsTotal(list) {
    var pl = list.post.length;
    var sl = list.shared.length;
    var cl = list.comment.length;

    return pl + sl + cl;
}

function addListenerHomeBtn(tabid){
    var a = document.querySelectorAll('a:not([target="_blank"]):not([rel="toggle"])');
    
    for(var x = 0; x < a.length; x++){
        a[x].addEventListener("click", function(){
            temp_hidden_elem.post = [];
            temp_hidden_elem.shared = [];
            temp_hidden_elem.comment = [];
        });
    }
    
}

function showFilteredPost(index, toShow, type, tabid) {
    var fpost = getFilteredPostAndAddToShow(index, toShow, type, tabid);
    if (toShow) {
        fpost.elem.setAttribute('data-inlist', 'yes');
        fpost.elem.setAttribute('data-toshow', 'yes');
        fpost.elem.style.display = 'block';
        scrollToElem(fpost);
        
        if(type == 'comment'){
            if(fpost.sub_elem != null){
                fpost.sub_elem.setAttribute('data-inlist', 'yes');
                fpost.sub_elem.setAttribute('data-toshow', 'yes');
                fpost.sub_elem.style.display = 'block';
            }
            
        }
        
    } else {
        fpost.elem.setAttribute('data-inlist', 'yes');
        fpost.elem.setAttribute('data-toshow', 'no');
        fpost.elem.style.display = 'none';
        
        if(type == 'comment'){
            if(fpost.sub_elem != null){
                fpost.sub_elem.setAttribute('data-inlist', 'yes');
                fpost.sub_elem.setAttribute('data-toshow', 'no');
                fpost.sub_elem.style.display = 'none';
            }
            
        }
    }
}

function scrollToElem(post) {
    //var offset = getOffsetSum(elem);
    var offset = post.offset;
    document.body.scrollTop = offset.top - 50;
}

function getOffsetSum(elem) {
    var top = 0, left = 0;
    while(elem) {
        //if(elem==document.getElementsByTagName('body')[0]){break}
        top = top + parseInt(elem.offsetTop);
        left = left + parseInt(elem.offsetLeft);
        elem = elem.offsetParent;
    }
    return { top: top, left: left };
}

function getFilteredPostAndAddToShow(index, value, type, tabid) {
    if(type == 'post'){
        temp_hidden_elem.post[index].toShow = value;
        return temp_hidden_elem.post[index];
    }else if(type == 'shared'){
        temp_hidden_elem.shared[index].toShow = value;
        return temp_hidden_elem.shared[index];
    }else if(type == 'comment'){
        temp_hidden_elem.comment[index].toShow = value;
        return temp_hidden_elem.comment[index];
    }
    
}

function filter() {
    chrome.storage.sync.get(['fbpost','shared','comment'], function (items) {
        var filters = [];
        var s_filters = [];
        var c_filters = [];
        if (typeof items.fbpost !== 'undefined') {
            filters = items.fbpost;
        }
        if (typeof items.shared !== 'undefined') {
            s_filters = items.shared;
        }
        if (typeof items.comment !== 'undefined') {
            c_filters = items.comment;
        }
        
        if (typeof filters !== 'undefined' && filters.length > 0
            || typeof s_filters !== 'undefined' && s_filters.length > 0
            || typeof c_filters !== 'undefined' && c_filters.length > 0) {
            //var container = document.querySelectorAll('._4ikz[data-referrer*="substream_0"]');
            //var container = document.querySelectorAll('._4ikz');
            var container = document.querySelectorAll('div._5jmm[data-testid*="fbfeed_story"]');
            //var container = document.getElementsByClassName('userContentWrapper');
            if(container.length > 0){
                for (var x = 0; x < container.length; x++) {
                    var _this = container[x];
                    
                    var checkHasShared = null;
                    filterPost(filters, _this);
                    filterSharedPost(s_filters, _this);
                    
                    var collapse_con = _this.getElementsByClassName('uiCollapsedList');
                    
                    if(collapse_con.length > 0){
                        for(var z = 0; z < collapse_con.length; z++){
                        var li = collapse_con[z].querySelectorAll('li > div[data-dedup]');
                            for(var y = 0; y < li.length; y++){
                                filterPost(filters, li[y], true);
                                filterSharedPost(s_filters, li[y], true);
                            }
                        }
                    }
                    
                    // if(collapse_con.length == 0){
                    //     filterPost(filters, _this);
                    //     //if(checkHasShared == null){
                    //     checkHasShared = filterSharedPost(s_filters, _this);
                    //     //}
                    // }else{
                    //     if(checkHasShared == null){
                    //         filterSharedPost(s_filters, _this);
                    //     }
                    //     for(var z = 0; z < collapse_con.length; z++){
                    //         var li = collapse_con[z].querySelectorAll('li > div[data-dedup]');
                    //         for(var y = 0; y < li.length; y++){
                    //             filterPost(filters, li[y]);
                    //             filterSharedPost(s_filters, li[y]);
                    //         }
                    //     }
                    // }
                    
                    
                }
                filterComments(c_filters);
                
                //updateListToBG(port, tabid);
            }
            
            var fbprofile = document.getElementById('recent_capsule_container');
            if(fbprofile != null){
                container = fbprofile.querySelectorAll('div._4-u2[data-ft]');
                for (var x = 0; x < container.length; x++) {
                    var _this = container[x];
                    filterPost(filters, _this);
                    filterSharedPost(s_filters, _this);
                }
                filterComments(c_filters);
                
                //updateListToBG(port, tabid);
            }else{
                var fbpage = document.getElementById('pagelet_timeline_main_column');
                if(fbpage != null){
                    container = fbpage.querySelectorAll('div._4-u2[data-ft]');
                    for (var x = 0; x < container.length; x++) {
                        var _this = container[x].parentElement.parentElement;
                        filterPost(filters, _this);
                        filterSharedPost(s_filters, _this);
                    }
                    filterComments(c_filters);
                    
                    //updateListToBG(port, tabid);
                }else{
                    var fbpage2 = document.getElementById('pagelet_group_mall');
                    if(fbpage2 != null){
                        container = fbpage2.querySelectorAll('div._4-u2[data-ft]');
                        for (var x = 0; x < container.length; x++) {
                            var _this = container[x];
                            filterPost(filters, _this);
                            filterSharedPost(s_filters, _this);
                        }
                        filterComments(c_filters);
                        
                        //updateListToBG(port, tabid);
                    }
                }
                
            }
        }

    });
}

function updateListToBG(port, tabid){
    var counter = getFilteredPostsTotal(temp_hidden_elem);
    var resp = { list: temp_hidden_elem, counter: counter };
    port.postMessage();
}

function filterPost(filters, _this, in_collapse){
    if(filters.length > 0){
        var j_this = $(_this);
        //var captions = _this.querySelectorAll('.userContent');
        var captions = j_this.find('.userContent').not('.uiCollapsedList .userContent');

        if(in_collapse === true){
            //captions = _this.querySelectorAll('.uiCollapsedList .userContent');
            captions = j_this.find('.userContent');
        }
        // else{
        //     var collapse = _this.getElementsByClassName('uiCollapsedList');
        //     if(collapse.length > 0 && captions.length == 0){
        //         return false;
        //     }
        // }
        
        //for (var y = 0; y < captions.length; y++) {
        if(captions.length > 0){
            for(var a = 0; a < captions.length; a++){
                // var dom_text = captions[0].innerText;
                captions[a] = convertHash(captions[a]);

                var dom_text = captions[a].innerText;

                for (var z = 0; z < filters.length; z++) {
                    if (typeof filters[z] !== 'undefined') {
                        if (!isHidden(_this) && hasString(dom_text, filters[z]) && !_this.hasAttribute("data-inlist")) {
                            var index = temp_hidden_elem.post.length;
                            _this.setAttribute('data-inlist', 'yes');
                            _this.setAttribute('data-toshow', 'no');
                            _this.setAttribute('data-post', filters[z].id);
                            var offset = getOffsetSum(_this);
                            var data = { 'index': index, 
                                        'id': filters[z].id, 
                                        'elem': _this, 
                                        'txt': dom_text, 
                                        'toShow': false, 
                                        'offset' : offset };
                                        
                            temp_hidden_elem.post.push(data);
                            _this.style.display = 'none';
                        }
                    }
                }
            }
        } 
        //}
        
        ////updateListToBG(port, tabid);
    }
}

function filterSharedPost(s_filters, _this, in_collapse){
    var data_length = temp_hidden_elem.shared.length;
    if(s_filters.length > 0){
        var j_this = $(_this);
        //var s1 = _this.querySelector(':not(.uiCollapsedList)').querySelectorAll('div.mtm[data-ft]')[0];
        //var s2 = _this.querySelector(':not(.uiCollapsedList)').querySelectorAll('div._6m3')[0];
        var s1 = j_this.find('div.mtm[data-ft]').not('.uiCollapsedList div.mtm[data-ft]')[0];
        var s2 = j_this.find('div._6m3').not('.uiCollapsedList div._6m3')[0];

        if(in_collapse === true){
            //s1 = _this.querySelectorAll('div.mtm[data-ft]')[0];
            //s2 = _this.querySelectorAll('div._6m3')[0];
            s1 = j_this.find('div.mtm[data-ft]')[0];
            s2 = j_this.find('div._6m3')[0];
        }
        
        s1 = convertHash(s1);
        s2 = convertHash(s2);

        var dom_text = '';
        var dom_text2 = '';
        
        if(typeof s1 !== 'undefined'){
            dom_text = s1.innerText;
        }
        if(typeof s2 !== 'undefined'){
            dom_text2 = s2.innerText;
        }
        
        if(dom_text != ''){
            loopFilterShared(s_filters, dom_text, _this, data_length);
        }
        if(dom_text2 != ''){
            loopFilterShared(s_filters, dom_text2, _this, data_length);
        }
        ////updateListToBG(port, tabid);
    }
    
    return data_length < temp_hidden_elem.shared.length;
}

function loopFilterShared(s_filters, dom_text, _this, data_length){
    for (var z = 0; z < s_filters.length; z++) {
        if (typeof s_filters[z] !== 'undefined') {
            if (!isHidden(_this) && hasString(dom_text, s_filters[z]) && !_this.hasAttribute("data-inlist")) {
                var index = data_length;
                _this.setAttribute('data-inlist', 'yes');
                _this.setAttribute('data-toshow', 'no');
                _this.setAttribute('data-shared', s_filters[z].id);
                var offset = getOffsetSum(_this);
                temp_hidden_elem.shared.push({ 'index': index, 'id': s_filters[z].id, 'elem': _this, 'txt': dom_text, 'toShow': false, 'offset' : offset });
                _this.style.display = 'none';
            }
            
            // if (!isHidden(_this) && hasString(dom_text2, s_filters[z]) && !_this.hasAttribute("data-inlist")) {
            //     var index = temp_hidden_elem.shared.length;
            //     _this.setAttribute('data-inlist', 'yes');
            //     _this.setAttribute('data-toshow', 'no');
            //     temp_hidden_elem.shared.push({ 'index': index, 'id': s_filters[z].id, 'elem': _this, 'txt': dom_text2, 'toShow': false });
            //     _this.style.display = 'none';
            // }
        }
    }
}

function filterComments(c_filters){
    if(c_filters.length > 0){
        var container = document.getElementsByClassName('UFIComment');
    
        for(var x = 0; x < container.length; x++){
            var _this = container[x];
            //var parent = _this.parentElement;
            var parent = _this;
            
            var comment_cont = _this.getElementsByClassName('UFICommentBody')[0];
            var dom_text = comment_cont.innerText;
            
            for (var z = 0; z < c_filters.length; z++) {
                if (typeof c_filters[z] !== 'undefined') {
                    if (!isHidden(parent) && hasString(dom_text, c_filters[z]) && !parent.hasAttribute("data-inlist")) {
                        var index = temp_hidden_elem.comment.length;
                        parent.setAttribute('data-inlist', 'yes');
                        parent.setAttribute('data-toshow', 'no');
                        parent.setAttribute('data-comment', c_filters[z].id);
                        var offset = getOffsetSum(parent);
                        
                        var sub_elem = null;
                        var reply_list = _this.nextSibling;
                        if(typeof reply_list !== 'undefined' && reply_list instanceof HTMLElement && reply_list.getAttribute("class") == ' UFIReplyList'){
                            sub_elem = reply_list;
                            sub_elem.style.display = 'none';
                            sub_elem.setAttribute('data-inlist', 'yes');
                            sub_elem.setAttribute('data-toshow', 'no');
                            sub_elem.setAttribute('data-comment', c_filters[z].id);
                        }
                        
                        temp_hidden_elem.comment.push({ 'index': index, 'id': c_filters[z].id, 'elem': parent, 'sub_elem': sub_elem, 'txt': dom_text, 'toShow': false, 'offset' : offset });
                        parent.style.display = 'none';
                    }
                }
            }
            
        }
        ////updateListToBG(port, tabid);
    }
    
}

function isHidden(el) {
    var style = window.getComputedStyle(el);
    return (style.display === 'none')
}

function removeFiltersByType(type, tabid){
    if(type == 'fbpost'){
        for (var x = 0; x < temp_hidden_elem.post.length; x++) {
            temp_hidden_elem.post[x].elem.removeAttribute('data-inlist');
            temp_hidden_elem.post[x].elem.removeAttribute('data-toshow');
            temp_hidden_elem.post[x].elem.style.display = 'block';
            
            var id = temp_hidden_elem.post[x].id;
            var target = document.querySelectorAll('[data-post="'+id+'"]');
            undoElementByDOM(target);
            
        }
        temp_hidden_elem.post = [];
    }else
    if(type == 'shared'){
        for (var x = 0; x < temp_hidden_elem.shared.length; x++) {
            temp_hidden_elem.shared[x].elem.removeAttribute('data-inlist');
            temp_hidden_elem.shared[x].elem.removeAttribute('data-toshow');
            temp_hidden_elem.shared[x].elem.style.display = 'block';
            
            var id = temp_hidden_elem.shared[x].id;
            var target = document.querySelectorAll('[data-shared="'+id+'"]');
            undoElementByDOM(target);
        }
        temp_hidden_elem.shared = [];
    }else
    if(type == 'comment'){
        for (var x = 0; x < temp_hidden_elem.comment.length; x++) {
            temp_hidden_elem.comment[x].elem.removeAttribute('data-inlist');
            temp_hidden_elem.comment[x].elem.removeAttribute('data-toshow');
            temp_hidden_elem.comment[x].elem.style.display = 'block';
            
            var id = temp_hidden_elem.comment[x].id;
            var target = document.querySelectorAll('[data-comment="'+id+'"]');
            undoElementByDOM(target);
        }
        temp_hidden_elem.comment = [];
    }
}

function undoElementByDOM(target){
    for(var x = 0; x < target.length; x++){
        target[x].removeAttribute('data-inlist');
        target[x].removeAttribute('data-toshow');
        target[x].style.display = 'block';
    }
    
}

function deleteAFilter(id, type, tabid) {
    var indices = [];
    if(type == 'post'){
        // for (var x = 0; temp_hidden_elem.post.length != 0; x = 0) {
       for (var x = 0; x < temp_hidden_elem.post.length; x++) {
            if (temp_hidden_elem.post[x].id == id) {
                temp_hidden_elem.post[x].elem.removeAttribute('data-inlist');
                temp_hidden_elem.post[x].elem.removeAttribute('data-toshow');
                temp_hidden_elem.post[x].elem.removeAttribute('data-post');
                temp_hidden_elem.post[x].elem.style.display = 'block';
                indices.push(x);
            }
        }
        
        var target = document.querySelectorAll('[data-post="'+id+'"]');
        undoElementByDOM(target);
        
        for(var y = indices.length - 1; y >= 0; y--){
            temp_hidden_elem.post.splice(indices[y], 1);
        }
    }else
    if(type == 'shared'){
        // for (var x = 0; temp_hidden_elem.shared.length != 0; x = 0) {
        for (var x = 0; x < temp_hidden_elem.shared.length; x++) {
            if (temp_hidden_elem.shared[x].id == id) {
                temp_hidden_elem.shared[x].elem.removeAttribute('data-inlist');
                temp_hidden_elem.shared[x].elem.removeAttribute('data-toshow');
                temp_hidden_elem.shared[x].elem.removeAttribute('data-shared');
                temp_hidden_elem.shared[x].elem.style.display = 'block';
                indices.push(x);
            }
        }
        
        var target = document.querySelectorAll('[data-shared="'+id+'"]');
        undoElementByDOM(target);
        
        for(var y = indices.length - 1; y >= 0; y--){
            temp_hidden_elem.shared.splice(indices[y], 1);
        }
    }else
    if(type == 'comment'){
        // for (var x = 0; temp_hidden_elem.comment.length != 0; x = 0) {
        for (var x = 0; x < temp_hidden_elem.comment.length; x++) {
            if (temp_hidden_elem.comment[x].id == id) {
                temp_hidden_elem.comment[x].elem.removeAttribute('data-inlist');
                temp_hidden_elem.comment[x].elem.removeAttribute('data-toshow');
                temp_hidden_elem.comment[x].elem.removeAttribute('data-comment');
                temp_hidden_elem.comment[x].elem.style.display = 'block';
                //temp_hidden_elem.comment.splice(x, 1);
                indices.push(x);
            }
            
            if(temp_hidden_elem.comment[x].sub_elem != null){
                temp_hidden_elem.comment[x].sub_elem.setAttribute('data-inlist', 'yes');
                temp_hidden_elem.comment[x].sub_elem.setAttribute('data-toshow', 'yes');
                temp_hidden_elem.comment[x].elem.removeAttribute('data-comment');
                temp_hidden_elem.comment[x].sub_elem.style.display = 'block';
            }
        }
        
        var target = document.querySelectorAll('[data-comment="'+id+'"]');
        undoElementByDOM(target);
        
        for(var y = indices.length - 1; y >= 0; y--){
            temp_hidden_elem.comment.splice(indices[y], 1);
        }
    }
}

function undoAllFilter(tabid) {
    for (var x = 0; x < temp_hidden_elem.post.length; x++) {
        temp_hidden_elem.post[x].elem.removeAttribute('data-inlist');
        temp_hidden_elem.post[x].elem.removeAttribute('data-toshow');
        temp_hidden_elem.post[x].elem.removeAttribute('data-comment');
        temp_hidden_elem.post[x].elem.style.display = 'block';
    }
    for (var x = 0; x < temp_hidden_elem.shared.length; x++) {
        temp_hidden_elem.shared[x].elem.removeAttribute('data-inlist');
        temp_hidden_elem.shared[x].elem.removeAttribute('data-toshow');
        temp_hidden_elem.shared[x].elem.removeAttribute('data-comment');
        temp_hidden_elem.shared[x].elem.style.display = 'block';
    }
    for (var x = 0; x < temp_hidden_elem.comment.length; x++) {
        temp_hidden_elem.comment[x].elem.removeAttribute('data-inlist');
        temp_hidden_elem.comment[x].elem.removeAttribute('data-toshow');
        temp_hidden_elem.comment[x].elem.removeAttribute('data-comment');
        temp_hidden_elem.comment[x].elem.style.display = 'block';
    }

    temp_hidden_elem.post = [];
    temp_hidden_elem.shared = [];
    temp_hidden_elem.comment = [];
}

function convertHash(dom){
    if(typeof dom !== 'undefined'){
        for(var x = 0; x < dom.querySelectorAll('span[aria-label="hashtag"]').length; x++){
            dom.querySelectorAll('span[aria-label="hashtag"]')[x].innerHTML = '#';
        }
    }
    return dom;
}

function convertHashJquery(dom){
    if(typeof dom !== 'undefined'){
        for(var x = 0; x < dom.find('span[aria-label="hashtag"]').length; x++){
            dom.find('span[aria-label="hashtag"]')[x].html('#');
        }
    }

    return dom;
}


function undo_filter() {

}

//dapat naa settings if case sensitive or not. default: dili case sensitive
function hasString(str, filter_obj) {
    //var lstr = str.toLowerCase();
    var lstr = str.trim();
    //var lcompare = compare.toLowerCase();
    var lcompare = filter_obj.filter;
    //var settings = filter_obj.settings;
    
    if(!filter_obj.case_sen){
        lstr = lstr.toLowerCase();
        lcompare = lcompare.toLowerCase();
    }
    
    if(filter_obj.word_sep){
        //var regex = new RegExp('\\b' + lcompare + '\\b');
        //return lstr.search(regex) > -1;
        return hasExactMatch(lstr,lcompare);
    }
    
    return lstr.indexOf(lcompare) > -1;
}

function testxhr() {
    $('.userContentWrapper').load(function () {
        filter();
    });
}

function matchExact(r, str) {
   try{
       var match = str.match(r);
   }catch(e){
       
   }
   
   return match != null && str == match[0];
}

function hasExactMatch(r, str) {
   var temp = r.split(/\s/);
   
   for(var x = 0; x < temp.length; x++){
       if(matchExact(temp[x], str)){
           return true;
       }
   }
}