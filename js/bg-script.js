var hidden_list = [];

//init();

function init(){
    chrome.tabs.query({ active: true }, function (tabs) {
        var port = chrome.tabs.connect(tabs[0].id, {name: "fbfilter"});
        port.postMessage({ comm: "init list storage", tabid: tabs[0].id });
        // setInterval(function () {
        //     //chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        //         getList(port, tabs);
        //     //});
        // }, 1000);
        
        // port.onMessage.addListener(function(response) {
        //     if(typeof response.list !== 'undefined'){
        //         var total = response.counter;//setFilteredPostsCounter(response.list);
        //         if (total > 0) {
        //             chrome.browserAction.setBadgeText({ text: total + '' });
        //         } else {
        //             chrome.browserAction.setBadgeText({ text: '' });
        //         }
        //     }
        // });
        
    });
}

// function getList(port, tabs){
//     //var port = chrome.tabs.connect(tabs[0].id, {name: "fbfilter"});
//     port.postMessage({ comm: "list update", tabid: tabs[0].id });
//     port.onMessage.addListener(function(response) {
//         var total = setFilteredPostsCounter(response.list);
//         if (total > 0) {
//             chrome.browserAction.setBadgeText({ text: total + '' });
//         } else {
//             chrome.browserAction.setBadgeText({ text: '' });
//         }
//     });
// }

function setFilteredPostsCounter(list) {
    var pl = list.post.length;
    var sl = list.shared.length;
    var cl = list.comment.length;

    return pl + sl + cl;
}

function addToFilteredPosts() {

}

// chrome.runtime.onMessage.addListener(
//   function(request, sender, sendResponse) {

//     if (request.bg_comm == "hidden list"){
//         console.log(hidden_list);
//         sendResponse({list: hidden_list});
//     }

//   });

refresh_fbpage();
checkUrl();
chrome.tabs.onActivated.addListener(function () {
    checkUrl();
});

chrome.webNavigation.onBeforeNavigate.addListener(function () {
    checkUrl();
});

chrome.windows.onFocusChanged.addListener(function (){
    checkUrl();
});

function checkUrl() {
    chrome.tabs.getSelected(null, function (tab) {
        var tablink = extractDomain(tab.url);
        if (tablink.indexOf('facebook.com') == -1) {
            chrome.browserAction.setIcon({ path: 'images/icon141x141-2-d.png' });
        } else {
            chrome.browserAction.setIcon({ path: 'images/icon141x141-2.png' });
            //getList(tab);
            console.log('grrr')
            init();
        }
    });
}

function extractDomain(url) {
    var domain;
    //find & remove protocol (http, ftp, etc.) and get domain
    if (url.indexOf("://") > -1) {
        domain = url.split('/')[2];
    }
    else {
        domain = url.split('/')[0];
    }

    //find & remove port number
    domain = domain.split(':')[0];

    return domain;
}

function refresh_fbpage(){
    chrome.runtime.onInstalled.addListener(function(obj){
        chrome.tabs.query({ active: false }, function (tabs) {
            for(var x = 0; x < tabs.length; x++){
                var tablink = extractDomain(tabs[x].url);
                if(tablink.indexOf('facebook.com') > 0){
                    chrome.tabs.reload(tabs[x].id);
                }
            }
        });
    });
}