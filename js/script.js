$(document).ready(function () {
	init();
});

function init() {
	$('#add-filter').click(function () {
		$('.add-filter-container').slideDown('fast');
		$('html, body').css('overflow-y','hidden');
	});

	$('#cancel-btn').click(function () {
		$('#filter-text').val('');
		$('.add-filter-container').slideUp('fast');
		$('html, body').css('overflow-y','auto');
	});

	$('#how-it-works').click(function(){
		chrome.tabs.create({ 'url': 'chrome-extension://' + chrome.runtime.id + '/howitworks.html' });
	});
}

var app = angular.module('FBUltimateFilter', [
	'jcs-autoValidate',
	'ngAnimate',
	'ui.router',
	'toaster',
	'ngSanitize',
	'ngMaterial'
]);

app.config(function ($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('list', {
			url: "/",
			templateUrl: 'templates/home.html',
			controller: 'FilterListController'
		})
		.state('hidden-list', {
			url: "/hidden-list",
			templateUrl: 'templates/filtered.html',
			controller: 'HiddenListController'
		})
		.state('settings', {
			url: "/settings",
			templateUrl: 'templates/settings.html',
			controller: 'SettingsController'
		})
		.state('notallowed',{
			url: "/notallowed",
			templateUrl: 'templates/notallowed.html'
		});

	$urlRouterProvider.otherwise('/');
});

app.run(function($state) {
	verifyLicense();
    chrome.tabs.getSelected(null,function(tab) {
		var tablink = extractDomain(tab.url);
		if(tablink.indexOf('facebook.com') == -1){
			$state.go('notallowed');
		}
	});
	
	function extractDomain(url) {
		var domain;
		//find & remove protocol (http, ftp, etc.) and get domain
		if (url.indexOf("://") > -1) {
			domain = url.split('/')[2];
		}
		else {
			domain = url.split('/')[0];
		}

		//find & remove port number
		domain = domain.split(':')[0];

		return domain;
	}

	function verifyLicense(){
		var token = chrome.identity.getAuthToken();
		//console.log(token)
		var CWS_LICENSE_API_URL = 'https://www.googleapis.com/chromewebstore/v1.1/userlicenses/';
		var req = new XMLHttpRequest();
		req.open('GET', CWS_LICENSE_API_URL + chrome.runtime.id);
		req.setRequestHeader('Authorization', 'Bearer ' + token);
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				var license = JSON.parse(req.responseText);
				verifyAndSaveLicense(license);
				console.log(license);
			}
		}
		req.send();
	}

	function verifyAndSaveLicense(license){
		var licenseStatus;
		var trialPeriodDays = 7;

		if (license.result && license.accessLevel == "FULL") {
			console.log("Fully paid & properly licensed.");
			licenseStatus = "FULL";
		} else if (license.result && license.accessLevel == "FREE_TRIAL") {
			var daysAgoLicenseIssued = Date.now() - parseInt(license.createdTime, 10);
			daysAgoLicenseIssued = daysAgoLicenseIssued / 1000 / 60 / 60 / 24;
			if (daysAgoLicenseIssued <= trialPeriodDays) {
				console.log("Free trial, still within trial period");
				licenseStatus = "FREE_TRIAL";
			} else {
				console.log("Free trial, trial period expired.");
				licenseStatus = "FREE_TRIAL_EXPIRED";
			}
		} else {
			console.log("No license ever issued.");
			licenseStatus = "NONE";
		}
	}

});

app.directive('tooltip', function(){
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            $(element).hover(function(){
                // on mouseenter
                $(element).tooltip('show');
            }, function(){
                // on mouseleave
                $(element).tooltip('hide');
            });
        }
    };
});

app.controller('NavController', function($scope, $state){
	$scope.isNotAllowed = false;
	chrome.tabs.getSelected(null,function(tab) {
		var tablink = $scope.extractDomain(tab.url);
		$scope.isNotAllowed = tablink.indexOf('facebook.com') == -1;
	});
	
	$scope.goTo = function(link){
		$state.go(link);
	}

	$scope.extractDomain = function(url){
		var domain;
		//find & remove protocol (http, ftp, etc.) and get domain
		if (url.indexOf("://") > -1) {
			domain = url.split('/')[2];
		}
		else {
			domain = url.split('/')[0];
		}

		//find & remove port number
		domain = domain.split(':')[0];

		return domain;
	}
});

app.controller('SettingsController', function ($scope, SettingsService) {
	$scope.user = SettingsService;

	$scope.setSettings = function (new_setting) {
		$scope.user.setSettings(new_setting)
			.then(function () {
				//Nothing to do here
			});
	}

	$scope.backDefault = function (ev) {
		$scope.user.backToDefaultSettings(ev);
	}
});

app.controller('HiddenListController', function ($scope, $interval, $sce, PostFiltersService) {
	init();
	$scope.hidden_list = [];
	$scope.list_len = 0;
	$scope.isLoading = true;
	$scope.hide = true;

	$interval(function () {
		
		// chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
			
		// 	chrome.tabs.sendMessage(tabs[0].id, { comm: "list update" }, function (response) {
		// 		if ($scope.isLoading) {
		// 			$scope.isLoading = false;
		// 		}
		// 		// if (response.list.length > 0) {
		// 		// 	$scope.hidden_list = response.list;
		// 		// 	$scope.list_len = $scope.hidden_list.length;
		// 		// }
		// 		console.log(response)
		// 		$scope.hidden_list = response.list;
		// 		$scope.list_len = $scope.listTotal(response.list);
		// 	});
		// });
		
		chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
			var port = chrome.tabs.connect(tabs[0].id, {name: "fbfilter"});
			port.postMessage({ comm: "list update", tabid: tabs[0].id });
			port.onMessage.addListener(function(response) {
				if ($scope.isLoading) {
					$scope.isLoading = false;
				}
				$scope.hidden_list = response.list;
				$scope.list_len = $scope.listTotal(response.list);
			});	
		});
		
	}, 1000);
	
	$scope.listTotal = function(list){
		var pl = list.post.length;
		var sl = list.shared.length;
		var cl = list.comment.length;
		
		return pl + sl + cl;
	}
	
	$scope.fpost = PostFiltersService;
	
	$scope.showToFB = function (index, type) {
		$('#post' + index).hide();
		$('#posth' + index).show();
		
		if(type == 'post'){
			$scope.hidden_list.post[index].toShow = true;
		}else if(type == 'shared'){
			$scope.hidden_list.shared[index].toShow = true;
		}else if(type == 'comment'){
			$scope.hidden_list.comment[index].toShow = true;
		}

		$scope.fpost.doCommand({ comm: "show filtered post", index: index, type: type }, false);
		// chrome.runtime.sendMessage({comm: "show filtered post", index: index}, function(response) {
		// 	//nothing to do here
		// });
	}

	$scope.hideToFB = function (index, type) {
		$('#posth' + index).hide();
		$('#post' + index).show();
		
		if(type == 'post'){
			$scope.hidden_list.post[index].toShow = false;
		}else if(type == 'shared'){
			$scope.hidden_list.shared[index].toShow = false;
		}else if(type == 'comment'){
			$scope.hidden_list.comment[index].toShow = false;
		}
		
		$scope.fpost.doCommand({ comm: "hide filtered post", index: index, type: type }, false);
	}

	$scope.highLightKeyWord = function (post, type) {
		var f = $scope.fpost.getFilter(post.id, type);
		var txt = $scope.getTxtBySettings(f, post.txt);
		//var index = txt.indexOf(f.filter);
		//var index = $scope.getIndexBySettings(f, txt);
		var html = '<span class="fword">';
		var ehtml = '</span>';
		var filter = $scope.getFilterBySettings(f);

		return $sce.trustAsHtml(txt.replace(filter, html + f.filter + ehtml));
	}
	
	$scope.getFilterBySettings = function(post_obj){
		//var settings = post_obj.settings;
		var filter = post_obj.filter;
		
		if(!post_obj.case_sen){
			filter = filter.toLowerCase();
		}
		
		if(post_obj.word_sep){
			var regex = new RegExp('\\b' + filter + '\\b');
			return regex;
		}
		
        return filter;
	}
	
	$scope.getTxtBySettings = function(post_obj, txt){
		//var settings = post_obj.settings;
		
		if(!post_obj.case_sen){
			return txt.toLowerCase();
		}
		
        return txt;
	}
});


app.controller('FilterListController', function ($scope, PostFiltersService, SettingsService, toaster) {
	init();
	$scope.posts = PostFiltersService;
	$scope.shared_post_filters = [];
	$scope.comment_filters = [];
	$scope.user_settings = SettingsService;
	$scope.filter = {type: 'fbpost'};
	$scope.filter.type = {'0' : {checked: true, label: 'fbpost'},
				   '1' : {checked: false, label: 'shared'},
				   '2' : {checked: false, label: 'comment'}};
	$scope.hasSelectedType = true;
	
	$scope.addFilter = function (filter, settings) {
		$scope.posts.addFilter(filter, settings)
			.then(function () {
				//toaster.pop('success', 'Added filter "' + filter.filter + '"');
				toaster.pop({
					type: 'success',
					body: 'Added filter "' + filter.filter + '"',
					showCloseButton: true
				});
				$scope.filter.filter = '';
				$scope.addFilterForm.$setPristine();
				$scope.posts.doFilter().then(function () {
					
				});
			}, function(){
				
			});
	}
	
	$scope.clearByType = function(ev, type){
		$scope.posts.clearByType(ev, type);
	}
	
	$scope.updateSubBtn = function(){
		$scope.hasSelectedType = $scope.checkHasSelectedType();
	}
	
	$scope.checkHasSelectedType = function(){
		for(var key in $scope.filter.type){
			if($scope.filter.type[key].checked){
				return true;
			}
		}
		
		return false;
	}
	
	$scope.deleteFilter = function (id, type) {
		$scope.posts.deleteFilter(id, type)
			.then(function () {

			});
	}

	$scope.clearAll = function () {
		$scope.posts.clearAll();
	}

	$('span[title]').tooltip();
});

app.service('SettingsService', function ($q, toaster, $mdDialog, $mdMedia) {
	var self = {
		'init': function () {
			self.loadSettings().then(function () {
				//nothing to do here
			});
		},
		'default_settings': { 'case_sen': false, 'word_sep': false },
		'settings': {},
		'setSettings': function (setting, from_default) {
			var d = $q.defer();
			chrome.storage.sync.set({ 'settings': setting }, function () {
				if (from_default) {
					//toaster.pop('success', 'Settings back to default.');
					toaster.pop({
						type: 'success',
						body: 'Settings back to default.',
						showCloseButton: true
					});
				} else {
					//toaster.pop('success', 'Saved new settings.');
					toaster.pop({
						type: 'success',
						body: 'Saved new settings.',
						showCloseButton: true
					});
				}

				d.resolve();
			});

			return d.promise;
		},
		'loadSettings': function () {
			var d = $q.defer();

			chrome.storage.sync.get('settings', function (items) {
				if (!jQuery.isEmptyObject(items)) {
					self.settings = items.settings;
				} else {
					self.settings = self.default_settings;
				}
				d.resolve();
			});

			return d.promise;
		},
		'backToDefaultSettings': function (ev) {
			self.confirmDialog('Back to Default Settings','Are you sure you want to get back to default settings?', ev)
				.then(function(){
					self.setSettings(self.default_settings, true)
						.then(function () {
							self.settings = self.default_settings;
						});
				});
		},
		'confirmDialog': function (title, msg, ev) {
			var d = $q.defer();
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
				.title(title)
				.textContent(msg)
				.targetEvent(ev)
				.ok('Yes')
				.cancel('No');
				
			$mdDialog.show(confirm).then(function () {
				d.resolve();
				//return true;
			}, function () {
				d.reject();
				//return false;
			});
			
			return d.promise;
		}
	};

	self.init();

	return self;
});

app.service('PostFiltersService', function ($q, toaster, $mdDialog, $mdMedia) {
	var self = {
		'init': function () {
			self.loadFilters().then(function () {
				self.initJQueryCodes();
			});
		},
		'extractDomain': function(url){
			var domain;
			//find & remove protocol (http, ftp, etc.) and get domain
			if (url.indexOf("://") > -1) {
				domain = url.split('/')[2];
			}
			else {
				domain = url.split('/')[0];
			}

			//find & remove port number
			domain = domain.split(':')[0];

			return domain;
		},
		'setCounter': function (counter) {
			// Disabled
			// if(counter == 0){
			// 	chrome.browserAction.setBadgeText({ text: "" });
			// }else{
			// 	chrome.browserAction.setBadgeText({ text: counter + "" });
			// }
			
		},
		'hasMore': true,
		'isLoading': false,
		'getFilter': function (id, type) {
			var filters = [];
			if(type == 'post'){
				filters = self.filters;
			}else if(type == 'shared'){
				filters = self.shared_filters;
			}else if(type == 'comment'){
				filters = self.comment_filters;
			}
			for (var i = 0; i < filters.length; i++) {
				var obj = filters[i];
				if (obj.id == id) {
					return obj;
				}

			}
		},
		'filters': [], //fbpost
		'shared_filters': [],
		'comment_filters': [],
		'search': null,
		'loadFilters': function () {
			var d = $q.defer();
			//if (self.hasMore && !self.isLoading) {
				//self.isLoading = true;

				chrome.storage.sync.get(['fbpost','shared','comment'], function (items) {
					// angular.forEach(items, function (i) {
					// 	self.filters.push(i);
					// });
					
					if (typeof items.fbpost !== 'undefined') {
						self.filters = items.fbpost;
					}
					
					if (typeof items.shared !== 'undefined') {
						self.shared_filters = items.shared;
					}
					
					if (typeof items.comment !== 'undefined') {
						self.comment_filters = items.comment;
					}
					//self.hasMore = false;
					//self.isLoading = false;
					d.resolve();
				});

				return d.promise;
			//}
		},
		'loadSharedFilters': function () {
			var d = $q.defer();
			//if (self.hasMore && !self.isLoading) {
				//self.isLoading = true;

				chrome.storage.sync.get('shared', function (items) {
					// angular.forEach(items, function (i) {
					// 	self.filters.push(i);
					// });

					if (typeof items.fbpost !== 'undefined') {
						self.shared_filters = items.shared;
					}
					//self.hasMore = false;
					//self.isLoading = false;
					d.resolve();
				});

				return d.promise;
			//}
		},
		'loadCommentFilters': function () {
			var d = $q.defer();
			//if (self.hasMore && !self.isLoading) {
				//self.isLoading = true;

				chrome.storage.sync.get('fbpost', function (items) {
					// angular.forEach(items, function (i) {
					// 	self.filters.push(i);
					// });

					if (typeof items.fbpost !== 'undefined') {
						self.filters = items.fbpost;
					}
					//self.hasMore = false;
					//self.isLoading = false;
					d.resolve();
				});

				return d.promise;
			//}
		},
		'isSaving': false,
		'selectedFilter': null,
		'getInsertId': function(filter, type){
			switch(type.label){
				case 'fbpost':
					return parseInt(self.filters[self.filters.length - 1].id) + 1;
				
				case 'shared':
					return parseInt(self.shared_filters[self.shared_filters.length - 1].id) + 1;
				
				case 'comment':
					return parseInt(self.comment_filters[self.comment_filters.length - 1].id) + 1;
					
				default:
			}
			//parseInt(self.filters[self.filters.length - 1].id) + 1;
		},
		'addFilter': function (filter, settings) {
			var d = $q.defer();
			self.isSaving = true;
			
			for(var x = 0; x < Object.keys(filter.type).length; x++){
				if(!filter.type[x].checked){
					continue;
				}
				
				var id = 0;
				var type = filter.type[x];
				if (type.checked && !self.isEmpty(type.label)) {
					id = self.getInsertId(filter, type);
				}
				if (self.checkFilterExist(type.label, filter, settings)) {
					var type_name = '';
					if(type.label == 'fbpost'){
						type_name = 'Post Caption Filters';
					}else if(type.label == 'shared'){
						type_name = 'Shared Post Caption Filters';
					}else if(type.label == 'comment'){
						type_name = 'Comment Text Filters';
					}
					//toaster.pop('error', 'Filter "' + filter.filter + '" already been added in '+type_name+'s.');
					toaster.pop({
						type: 'error',
						body: 'Filter "' + filter.filter + '" already been added in '+type_name+'.',
						showCloseButton: true
					});
					
					d.reject();
				} else {
					//var new_filter = { 'id': id, 'filter': filter, 'settings': settings };
					//self.filters.push(new_filter);
					var new_filter = self.setNewFilter(type.label, id, filter, settings);

					chrome.storage.sync.set(new_filter, function () {
						d.resolve();
					});
				}
			}

			return d.promise;
		},
		'setNewFilter': function(type, id, filter, settings){
			var new_filter = { 'id': id, 'filter': filter.filter, 'case_sen': settings.case_sen, 'word_sep': settings.word_sep };
			switch(type){
				case 'fbpost':
					self.filters.push(new_filter);
					return {'fbpost' : self.filters};
				case 'shared':
					self.shared_filters.push(new_filter);
					return {'shared' : self.shared_filters};
				case 'comment':
					self.comment_filters.push(new_filter);
					return {'comment' : self.comment_filters};
				default:
					self.filters.push(new_filter);
					return {'fbpost' : self.filters};
			}
			
		},
		'deleteFilter': function (id, type) {
			var d = $q.defer();
			var selFilter = '';
			var filters = [];
			
			if(type == 'post'){
				filters = self.filters;
			}else if(type == 'shared'){
				filters = self.shared_filters;
			}else if(type == 'comment'){
				filters = self.comment_filters;
			}
			
			for (var x = 0; x < filters.length; x++) {
				if (filters[x].id == id) {
					selFilter = filters[x].filter;
					// delete self.filters[x];
					filters.splice(x, 1);
				}
			}
			
			if(type == 'post'){
				self.filters = filters;
				chrome.storage.sync.set({ 'fbpost': self.filters }, function () {
					d.resolve();
				});
			}else if(type == 'shared'){
				self.shared_filters = filters;
				chrome.storage.sync.set({ 'shared': self.shared_filters }, function () {
					d.resolve();
				});
			}else if(type == 'comment'){
				self.comment_filters = filters;
				chrome.storage.sync.set({ 'comment': self.comment_filters }, function () {
					d.resolve();
				});
			}
			
			self.doCommand({ comm: 'delete a filter', id: id, type: type }, true);
			//toaster.pop('success', 'Filter "' + selFilter + '" deleted.');
			toaster.pop({
				type: 'success',
				body: 'Filter "' + selFilter + '" deleted.',
				showCloseButton: true
			});
			return d.promise;
		},
		'clearByType': function(ev, type){
			var type_name = '';
			if(type == 'fbpost'){
				type_name = 'Post Caption Filters';
			}else if(type == 'shared'){
				type_name = 'Shared Post Caption Filters';
			}else if(type == 'comment'){
				type_name = 'Comment Text Filters';
			}
			
			self.confirmDialog('Delete Filters','Are you sure you want to delete all '+type_name+'?', ev)
				.then(function(){
					chrome.storage.sync.remove(type);
					if(type == 'fbpost'){
						self.filters = [];
					}else if(type == 'shared'){
						self.shared_filters = [];
					}else if(type == 'comment'){
						self.comment_filters = [];
					}
					self.selectedFilter = null;
					return self.doCommand({ comm: "remove filters by type", type: type }, true);
				});
		},
		'clearAll': function (ev) {
			self.confirmDialog('Delete All Filters','Are you sure you want to delete all filters?', ev)
				.then(function(){
					chrome.storage.sync.clear();
					self.filters = [];
					self.shared_filters = [];
					self.comment_filters = [];
					self.selectedFilter = null;
					return self.doCommand({ comm: "undo all filters" }, true);
				});
		},
		'checkFilterExist': function (type, new_filter, settings) {
			var filter = [];
			switch(type){
				case 'fbpost':
					filter = self.filters;
					break;
				case 'shared':
					filter = self.shared_filters;
					break;
				case 'comment':
					filter = self.comment_filters;
					break;
				default:
					filter = self.filters;
			}
			for (var x = 0; x < filter.length; x++) {
				if(settings.case_sen){
					if (filter[x].filter == new_filter.filter && filter[x].word_sep == settings.word_sep) {
						return true;
					}
				}else{
					if (filter[x].filter.toLowerCase() == (new_filter.filter).toLowerCase() && filter[x].word_sep == settings.word_sep) {
						return true;
					}
				}
				
				if(settings.word_sep){
					var regex = new RegExp('\\b' + new_filter.filter + '\\b');
					return filter[x].filter.search(regex) > -1;
				}
				
			}

			return false;
		},
		'isEmpty': function (type) {
			switch(type){
				case 'fbpost':
					return self.filters.length == 0;
				
				case 'shared':
					return self.shared_filters.length == 0;
				
				case 'comment':
					return self.comment_filters.length == 0;
					
				default:
			}
			//return self.filters.length == 0;
		},
		'doFilter': function () {
			return self.doCommand({ comm: "filter feeds" }, true);
		},
		'doCommand': function (req, toSetCounter) {
			var d = $q.defer();
			// console.log('test')
			// chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
			// 	chrome.tabs.sendMessage(tabs[0].id, req, function (response) {
			// 		console.log(response)
			// 		if (toSetCounter && typeof response !== 'undefined' && typeof response.counter !== 'undefined') {
			// 			self.setCounter(response.counter);
			// 		}
			// 		d.resolve();
			// 	});
			// });
			
			chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
				var port = chrome.tabs.connect(tabs[0].id, {name: "fbfilter"});
				req.tabid = tabs[0].id;
				port.postMessage(req);
				port.onMessage.addListener(function(response) {
					// if (toSetCounter && response != null && typeof response !== 'undefined' && typeof response.counter !== 'undefined') {
			 		// 	self.setCounter(response.counter);
			 		// }
			 		d.resolve();
				});	
			});

			return d.promise;
		},
		// 'getCounterUpdate': function () {
		// 	var d = $q.defer();
		// 	chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
		// 		chrome.tabs.sendMessage(tabs[0].id, req, function (response) {
		// 			self.setCounter(response.counter);
		// 			d.resolve();
		// 		});
		// 	});

		// 	return d.promise;
		// },
		'hasString': function (str, compare) {
			var lstr = str.toLowerCase();
			var lcompare = compare.toLowerCase();
			return lstr.indexOf(lcompare) > -1;
		},
		'initJQueryCodes': function () {
			$('#add-filter').click(function () {
				$('.add-filter-container').slideDown('fast');
			});

			$('#cancel-btn').click(function () {
				$('#filter-text').val('');
				$('.add-filter-container').slideUp('fast');
			});
		},
		'confirmDialog': function (title, msg, ev) {
			var d = $q.defer();
			// Appending dialog to document.body to cover sidenav in docs app
			var confirm = $mdDialog.confirm()
				.title(title)
				.textContent(msg)
				.targetEvent(ev)
				.ok('Yes')
				.cancel('No');
				
			$mdDialog.show(confirm).then(function () {
				d.resolve();
				//return true;
			}, function () {
				d.reject();
				//return false;
			});
			
			return d.promise;
		}

	};

	self.init();

	return self;
});