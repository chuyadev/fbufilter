var app = angular.module('PostService', [
	'toaster',
]);

app.service('PostFiltersService', function ($q, toaster) {
	var self = {
		'init': function(){
			self.loadFilters().then(function(){
				self.initJQueryCodes();
				//console.log('done');
				//self.setCounter(0);
			});
			// setTimeout(function(){
			// 	self.getCounterUpdate();
			// },1000);
		},
		'setCounter': function(counter){
			chrome.browserAction.setBadgeText({text: counter+""});	
		},
		'hasMore': true,
		'isLoading': false,
		'getFilter': function (id) {
			for (var i = 0; i < self.filters.length; i++) {
				var obj = self.filters[i];
				if (obj.id == id) {
					return obj;
				}

			}
		},
		'filters': [],
		'search': null,
		'loadFilters': function () {
			var d = $q.defer();
			if (self.hasMore && !self.isLoading) {
				self.isLoading = true;
				
				chrome.storage.sync.get('fbpost', function (items) {
					// angular.forEach(items, function (i) {
					// 	self.filters.push(i);
					// });
					
					if(typeof  items.fbpost !== 'undefined'){
						self.filters = items.fbpost;
					}
					self.hasMore = false;
					self.isLoading = false;
					d.resolve();
				});
				
				return d.promise;
			}
		},
		'removeFilter': function (person) {
			// var d = $q.defer();
			// self.isDeleting = true;
			// person.$remove().then(function () {
			// 	self.isDeleting = false;
			// 	var index = self.filters.indexOf(person);
			// 	self.filters.splice(index, 1);
			// 	self.selectedPerson = null;
			// 	toaster.pop('success', 'Deleted ' + person.name);
			// 	d.resolve()
			// });
			// return d.promise;
		},
		'isSaving': false,
		'selectedFilter': null,
		'addFilter': function (filter) {
			var d = $q.defer();
			self.isSaving = true;
			
			var id = 0;
			if(!self.isEmpty()){
				id = parseInt(self.filters[self.filters.length - 1].id) + 1;
			}
			
			if(self.checkFilterExist(filter)){
				toaster.pop('warning', 'Filter "' + filter +'" already been added.');
				d.resolve();
			}else{
				var new_filter = {'id' : id, 'filter' : filter};
				self.filters.push(new_filter);
				
				chrome.storage.sync.set({'fbpost': self.filters}, function(){
					self.isSaving = false;
					self.selectedFilter = null;
					//self.hasMore = true;
					//self.page = 1;
					//self.filters = [];
					//self.loadContacts();
					toaster.pop('success', 'Added filter "' + filter +'"');
					d.resolve();
				});
			}
			
			return d.promise;
		},
		'deleteFilter': function(id){
			var d = $q.defer();
			var selFilter = '';
			for(var x = 0; x < self.filters.length; x++){
				if(self.filters[x].id == id){
					selFilter = self.filters[x].filter;
					// delete self.filters[x];
					self.filters.splice(x,1);
				}
			}
			
			chrome.storage.sync.set({'fbpost': self.filters}, function(){
				self.isSaving = false;
				self.selectedFilter = null;
				//self.hasMore = true;
				//self.page = 1;
				//self.filters = [];
				//self.loadContacts();
				d.resolve();
			});
			self.doCommand({comm: 'delete a filter', id: id});
			toaster.pop('success', 'Filter "' + selFilter +'" deleted.');
			return d.promise;
		},
		'clearAll': function(){
			chrome.storage.sync.clear();
			self.filters = [];
			self.selectedFilter = null;
			return self.doCommand({comm: "undo all filters"});
		},
		'checkFilterExist': function(new_filter){
			for(var x = 0; x < self.filters.length; x++){
				if(self.filters[x].filter == new_filter){
					return true;
				}
			}
			
			return false;
		},
		'isEmpty': function(){
			return self.filters.length == 0;
		},
		'doFilter': function(){
			return self.doCommand({comm: "filter feeds"});
		},
		'doCommand': function(req){
			var d = $q.defer();
			chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
				chrome.tabs.sendMessage(tabs[0].id, req, function(response) {
					// console.log('from script js');
					// console.log(tabs)
					console.log(response);
					self.setCounter(response.counter);
					d.resolve();
				});
			});
			
			return d.promise;
		},
		'getCounterUpdate': function(){
			var d = $q.defer();
			chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
				chrome.tabs.sendMessage(tabs[0].id, req, function(response) {
					// console.log('from script js');
					// console.log(tabs)
					console.log(response);
					self.setCounter(response.counter);
					d.resolve();
				});
			});
			
			return d.promise;	
		},
		'hasString': function(str, compare){
			var lstr = str.toLowerCase();
			var lcompare = compare.toLowerCase();
			return lstr.indexOf(lcompare) > -1;
		},
		'initJQueryCodes': function(){
			$('#add-filter').click(function(){
				$('.add-filter-container').slideDown('fast');
			});

			$('#cancel-btn').click(function(){
				$('#filter-text').val('');
				$('.add-filter-container').slideUp('fast');
			});
		}

	};

	self.init();

	return self;
});